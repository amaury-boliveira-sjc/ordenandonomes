package br.com.ordenandonomes.model;

import java.io.Serializable;
import java.util.Arrays;

public class Name implements Serializable {

    private String[] names;

    public Name() {
        names = new String[0];
    }

    public String insertName(String name){
        String[] newNames = new String[names.length + 1];
        for (int i = 0; i < names.length; i++){
            newNames[i] = names[i];
        }
        newNames[names.length] = name;
        names = newNames;
        orderNames();
        return transformToString();
    }

    private void orderNames(){
        Arrays.sort(names);
    }

    private String transformToString(){
        return Arrays.toString(names);
    }
}
