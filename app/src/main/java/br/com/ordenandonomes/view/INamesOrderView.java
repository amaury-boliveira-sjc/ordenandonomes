package br.com.ordenandonomes.view;

public interface INamesOrderView {

    void showNames(String names);

    void showError(String error);
}
