package br.com.ordenandonomes.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import br.com.ordenandonomes.R;
import br.com.ordenandonomes.controller.NamesOrderPresenter;

public class NamesOrderActivity extends AppCompatActivity implements INamesOrderView, Button.OnClickListener {

    private EditText editText;
    private Button button;
    private TextView textView;
    private NamesOrderPresenter namesOrderPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.names_order_activity);

        initComponents();
        initListeners();
    }

    private void initComponents(){
        this.editText = findViewById(R.id.edit_text_names);
        this.button = findViewById(R.id.button_ok);
        this.textView = findViewById(R.id.text_view_array_names);
        namesOrderPresenter = new NamesOrderPresenter(this);
    }

    private void initListeners(){
        this.button.setOnClickListener(this);
    }

    @Override
    public void showNames(String names) {
        textView.setText(names);
        editText.getText().clear();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        namesOrderPresenter.orderingNames(editText.getText().toString());
    }
}
