package br.com.ordenandonomes.controller;

import br.com.ordenandonomes.model.Name;
import br.com.ordenandonomes.view.INamesOrderView;

public class NamesOrderPresenter {

    private INamesOrderView namesOrderView;
    private Name name;

    public NamesOrderPresenter(INamesOrderView namesOrderView) {
        this.namesOrderView = namesOrderView;
        this.name = new Name();
    }

    public void orderingNames(String name){
        String error = validate(name);
        if (error != null){
            namesOrderView.showError(error);
        }else{
            String names = this.name.insertName(name);
            namesOrderView.showNames(names);
        }
    }

    private String validate(String name){
        if (name != null && !name.equals("")){
            return null;
        }else{
            return "Campo obrigatório, por favor digite algum nome!";
        }
    }
}
